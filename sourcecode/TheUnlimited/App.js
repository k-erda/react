import 'react-native-gesture-handler';
import React, {Component} from 'react';
import SplashScreen from 'react-native-splash-screen';
import {connect} from 'react-redux';
import {updateUserFirstTimeBoardingStatus} from './src/redux/actions/authAction';
import {bindActionCreators} from 'redux';
import {
  SplashStackNavigator,
  OnBoardingIntroNavigator,
} from './src/navigations/UnlimitedNavigator';
import {SafeAreaView} from 'react-native';

class App extends Component {
  componentDidMount() {
    SplashScreen.hide();
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.props.isUserFirstTimeBoarding ? (
          <SplashStackNavigator />
        ) : (
          <SplashStackNavigator />
        )}
      </SafeAreaView>
    );
  }
}
/**
 * Method to save data in redux
 */
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      updateUserFirstTimeBoardingStatus,
    },
    dispatch,
  );

function mapStateToProps(state) {
  return {
    userLoggedInStatus: state.userAuth.userLoggedInStatus,
    isUserFirstTimeBoarding: state.userAuth.isUserFirstTimeBoarding,
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
