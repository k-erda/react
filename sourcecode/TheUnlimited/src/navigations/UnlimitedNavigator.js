import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Splash from '../pages/Splash';
import Home from '../pages/Home';
import MyProject from '../pages/MyProducts';
import Profile from '../pages/Profile';
import OnBoardingIntro from '../pages/OnBoardingIntro';

const SplashStack = createStackNavigator();
const Tab = createBottomTabNavigator();
const onBoardingIntroStack = createStackNavigator();

export function SplashStackNavigator() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="Splash"
        activeColor="red"
        // inactiveColor="#3e2465"
        labeled={true}
        // activeBackgroundColor={"red"}
        showLabel={false}
        tabStyle={{backgroundColor: 'grey'}}>
        <Tab.Screen
          options={{
            tabBarLabel: 'Splash',
            tabBarIcon: ({color}) => (
              <MaterialCommunityIcons name="home" color={color} />
            ),
          }}
          name="Splash"
          component={Splash}
        />

        <Tab.Screen
          options={{
            tabBarIcon: ({color}) => (
              <MaterialCommunityIcons name="bell" color={color} />
            ),
          }}
          name="Home"
          component={Home}
        />

        <Tab.Screen
          options={{
            tabBarIcon: ({color}) => (
              <MaterialCommunityIcons name="bell" color={color} />
            ),
          }}
          name="MyProject"
          component={MyProject}
        />
        <Tab.Screen
          options={{
            tabBarIcon: ({color}) => (
              <MaterialCommunityIcons name="bell" color={color} />
            ),
          }}
          name="Profile"
          component={Profile}
        />
      </Tab.Navigator>
      {/* <SplashStack.Navigator>
        <SplashStack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
      </SplashStack.Navigator> */}
    </NavigationContainer>
  );
}

export function OnBoardingIntroNavigator() {
  return (
    <NavigationContainer>
      <onBoardingIntroStack.Navigator>
        <onBoardingIntroStack.Screen
          name="OnBoardingIntro"
          component={OnBoardingIntro}
          options={{headerShown: false}}
        />
      </onBoardingIntroStack.Navigator>
    </NavigationContainer>
  );
}
