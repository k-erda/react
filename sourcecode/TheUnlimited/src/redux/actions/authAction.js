import {IS_USER_FIRST_TIME_BOARDING} from '../constants/index';

export const updateUserFirstTimeBoardingStatus = (status) => {
  return {
    type: IS_USER_FIRST_TIME_BOARDING,
    payload: status,
  };
};
