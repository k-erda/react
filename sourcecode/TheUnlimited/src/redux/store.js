import {persistReducer, persistStore} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import {createStore, applyMiddleware} from 'redux';
import {createLogger} from 'redux-logger';
import rootReducer from './reducers/index';

/* Redux persist configs */
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

// Middleware: Redux Saga
// Redux: Store
const store = createStore(persistedReducer, applyMiddleware(createLogger()));

export const persistor = persistStore(store);

export default store;
