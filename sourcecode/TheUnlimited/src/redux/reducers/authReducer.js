import {
  USER_LOGGED_IN_STATUS,
  IS_USER_FIRST_TIME_BOARDING,
} from '../constants/index';

const initialState = {
  userLoggedInStatus: false,
  isUserFirstTimeBoarding: true,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGGED_IN_STATUS: {
      return {
        ...state,
        userLoggedInStatus: action.payload,
      };
    }
    case IS_USER_FIRST_TIME_BOARDING: {
      return {
        ...state,
        isUserFirstTimeBoarding: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};

export default authReducer;
