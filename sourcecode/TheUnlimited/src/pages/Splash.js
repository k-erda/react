import React, {Component} from 'react';
import {View} from 'react-native';
import firebase from 'react-native-firebase';
import FacebookLoginController from '../components/FacebookLoginController';
import GoogleLoginController from '../components/GoogleLoginController';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from 'react-native-splash-screen';

class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  onLoginDone = (error, result) => {
    if (!error) {
      console.log(result);
    } else {
      console.log('Reason for error', result);
    }
  };

  async componentDidMount() {
    SplashScreen.hide();
    this.checkPermission();
  }

  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //3
  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
    console.log(fcmToken);
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  render() {
    return (
      <View>
        <GoogleLoginController
          onLoginDone={(error, result) => this.onLoginDone(error, result)}
        />
        <FacebookLoginController
          onLoginDone={(error, result) => this.onLoginDone(error, result)}
        />
      </View>
    );
  }
}
export default Splash;
