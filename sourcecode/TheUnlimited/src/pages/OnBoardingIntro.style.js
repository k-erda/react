import {StyleSheet} from 'react-native';
import * as AppConstants from '../utils/AppConstants';
import {scale, moderateScale, verticalScale} from '../utils/ScalingUnits';

export const Styles = StyleSheet.create({
  mainContainerStyle: {
    flex: 1,
    backgroundColor: 'white',
  },
  buttonContainerStyle: {
    backgroundColor: 'black',
  },
  buttonTextStyle: {
    color: 'white',
    fontFamily: AppConstants.FONT_GILROY_BOLD,
  },
});
