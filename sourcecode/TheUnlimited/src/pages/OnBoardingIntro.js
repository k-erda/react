import React, {Component} from 'react';
import {Dimensions, SafeAreaView} from 'react-native';
import IntroListSlider from '../components/IntroListItem';
import CustomButton from '../components/CustomButton';
import {Styles} from '../pages/OnBoardingIntro.style';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {updateUserFirstTimeBoardingStatus} from '../redux/actions/authAction';
class OnBoardingIntro extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          imageType: 1,
          title: 'Free Airtime/\nData Rewards',
          subTitle: 'Dummy text of typesetting',
        },
        {
          imageType: 2,
          title: 'Get Fast-food &\nMovie Tickets',
          subTitle: 'Dummy text of typesetting',
        },
        {
          imageType: 3,
          title: 'Free Spins, Reedem\nReward Points',
          subTitle: 'Dummy text of typesetting',
        },
      ],
    };
  }

  getStarted = () => {
    this.props.updateUserFirstTimeBoardingStatus(false);
  };

  render() {
    const screenWidth = Math.round(Dimensions.get('window').width);
    return (
      <SafeAreaView style={Styles.mainContainerStyle}>
        <IntroListSlider
          data={this.state.data}
          imageKey={'image'}
          local={false}
          width={screenWidth}
          separator={0}
          currentIndexCallback={(index) => console.log('Index', index)}
          indicator
        />
        <CustomButton
          onPress={this.getStarted()}
          buttonCaption={'GET STARTED'}
          buttonContainerStyle={Styles.buttonContainerStyle}
          buttonTextStyle={Styles.buttonTextStyle}
        />
      </SafeAreaView>
    );
  }
}

/**
 * Method to save data in redux
 */
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      updateUserFirstTimeBoardingStatus,
    },
    dispatch,
  );

function mapStateToProps(state) {
  return {
    userLoggedInStatus: state.userAuth.userLoggedInStatus,
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(OnBoardingIntro);
