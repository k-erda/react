import React, {Component} from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';
import * as AppConstants from '../utils/AppConstants';

export default class CustomButton extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <TouchableOpacity
        activeOpacity={0.6}
        style={[styles.buttomContainerStyle, this.props.buttonContainerStyle]}
        onPress={this.props.onPress}>
        <Text style={[styles.buttonTextStyle, this.props.buttonTextStyle]}>
          {this.props.buttonCaption}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  buttomContainerStyle: {
    height: 55,
    backgroundColor: 'black',
    marginTop: 54,
    width: 256,
    alignSelf: 'center',
    borderRadius: 5,
    justifyContent: 'center',
  },
  buttonTextStyle: {
    color: 'white',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: AppConstants.FONT_GILROY_BOLD,
  },
});
