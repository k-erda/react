import {StyleSheet, Dimensions} from 'react-native';
import * as AppConstants from '../utils/AppConstants';
import * as Colors from '../utils/Colors';
import {scale, moderateScale, verticalScale} from '../utils/ScalingUnits';

export const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'column',
    width: Dimensions.get('window').width,
  },
  imageContainer: {
    height: '70%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleTextStyle: {
    alignSelf: 'center',
    textAlign: 'center',
    fontFamily: AppConstants.FONT_GILROY_BOLD,
    color: Colors.intro_screen_title_color,
    fontSize: 24,
  },
  subTitleTextStyle: {
    fontFamily: AppConstants.FONT_GILROY_MEDIUM,
    color: Colors.intro_screen_subtitle_title_color,
    opacity: 0.6,
    fontSize: 16,
    marginTop: verticalScale(26),
    alignSelf: 'center',
    textAlign: 'center',
  },
  introImageStyle: {
    width: scale(167),
    height: scale(167),
  },
});
