import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';

class FacebookLoginController extends Component {
  state = {userInfo: {}};

  getInfoFromToken = (token) => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'id, name,  first_name, last_name',
      },
    };
    const profileRequest = new GraphRequest(
      '/me',
      {token, parameters: PROFILE_REQUEST_PARAMS},
      (error, result) => {
        if (error) {
          console.log('login info has error: ' + error);
          this.props.onLoginDone(true, error);
        } else {
          this.setState({userInfo: result});
          this.props.onLoginDone(false, result);
          console.log('result:', result);
        }
      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  };

  onLoginFinished = (error, result) => {
    if (error) {
      console.log('login has error: ' + result.error);
      this.props.onLoginDone(true, result.error);
    } else if (result.isCancelled) {
      console.log('login is cancelled.');
      this.props.onLoginDone(true, 'login is cancelled.');
    } else {
      AccessToken.getCurrentAccessToken().then((data) => {
        const accessToken = data.accessToken.toString();
        this.getInfoFromToken(accessToken);
      });
    }
  };

  render() {
    return (
      <View style={{flex: 1, margin: 50}}>
        <LoginButton
          onLoginFinished={(error, result) => {
            this.onLoginFinished(error, result);
          }}
          onLogoutFinished={() => this.setState({userInfo: {}})}
        />
      </View>
    );
  }
}
export default FacebookLoginController;
