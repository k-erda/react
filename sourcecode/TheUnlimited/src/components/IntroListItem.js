import React, {Component} from 'react';
import {Image, View, Text} from 'react-native';
import {scale} from '../utils/ScalingUnits';
import {styles} from './IntroListItem.style';

export default class IntroListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: props.item,
    };
  }

  renderIntroImage = (imageType) => {
    switch (imageType) {
      case 1:
        return (
          <Image
            resizeMode={'contain'}
            style={{width: scale(162), height: scale(162)}}
            source={require('../assets/images/intro_first_img.png')}
          />
        );
      case 2:
        return (
          <Image
            resizeMode={'contain'}
            style={{width: scale(184), height: scale(184)}}
            source={require('../assets/images/intro_second_img.png')}
          />
        );
      case 3:
        return (
          <Image
            resizeMode={'contain'}
            style={{width: scale(184), height: scale(184)}}
            source={require('../assets/images/intro_third_img.png')}
          />
        );
      default:
        break;
    }
  };

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.imageContainer}>
          {this.renderIntroImage(this.state.item.imageType)}
        </View>
        <Text style={styles.titleTextStyle}>{this.state.item.title}</Text>
        <Text style={styles.subTitleTextStyle}>{this.state.item.subTitle}</Text>
      </View>
    );
  }
}
